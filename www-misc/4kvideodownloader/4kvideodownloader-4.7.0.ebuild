# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit desktop

DESCRIPTION="Download videos from youtube with gui"
HOMEPAGE="https://www.4kdownload.com/"
SRC_URI="https://dl.4kdownload.com/app/4kvideodownloader_4.7.0-1_amd64.deb"

# LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="sys-libs/glibc"
RDEPEND="${DEPEND}"
BDEPEND="sys-devel/binutils"

src_unpack () {
	mkdir -p ${S}
	cp ${A} ${S}
	cd ${S}
	unpack ${A}
	tar xf data.tar.xz
}

src_prepare () {
	eapply ${FILESDIR}/${P}-fix-desktop-file.patch
	eapply_user
}

src_install () {
	echo ${S} > /dev/stderr
	cp -R ${S}/usr ${D}
	doicon -s 48x48 ${D}/usr/share/icons/4kvideodownloader.png
	rm ${D}/usr/share/icons/4kvideodownloader.png
	mkdir -p ${D}/usr/bin/
	ln -s /usr/lib/4kvideodownloader/4kvideodownloader.sh ${D}/usr/bin/4kvideodownloader
}
